import * as React from 'react';
import Svg, {Path} from 'react-native-svg';

const State = () => (
  <Svg width={3} height={22} fill="none" xmlns="http://www.w3.org/2000/svg">
    <Path fill="#AC5253" d="M-12-1h24v24h-24z" />
  </Svg>
);

export default State;
