import styled from 'styled-components/native';

export const StyledContainer = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  width: 100%;
  padding: 15px;
`;

export const StyledInput = styled.TextInput`
  font-size: 17px;
  margin-left: 10px;
`;
