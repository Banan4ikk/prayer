export interface ICommentFormProps {
  prayerId: number;
}

export interface ICommentValue {
  body: string;
}
