import React, {FC} from 'react';
import {Field, Form} from 'react-final-form';
import {StyledContainer, StyledInput} from './styles';
import {ICommentFormProps, ICommentValue} from './interfaces';
import {useDispatch} from 'react-redux';
import {actions} from '../../../redux/ducks';

const AddCommentForm: FC<ICommentFormProps> = ({prayerId}) => {
  const dispatch = useDispatch();
  const onSubmit = (comment: ICommentValue) => {
    const payload = {
      body: comment.body,
      prayerId,
    };
    dispatch({type: actions.comments.addComment.type, payload});
  };

  return (
    <Form
      onSubmit={onSubmit}
      render={({handleSubmit, form}) => (
        <StyledContainer>
          <Field name="body" placeholder="Add a comment...">
            {({input, placeholder}) => {
              return (
                <StyledInput
                  onChange={input.onChange}
                  placeholder={placeholder}
                  value={input.value}
                  onSubmitEditing={() => {
                    handleSubmit();
                    form.reset();
                  }}
                />
              );
            }}
          </Field>
        </StyledContainer>
      )}
    />
  );
};

export default AddCommentForm;
