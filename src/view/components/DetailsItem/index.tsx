import React, {FC} from 'react';
import {DetailsItemProps} from './interfaces';
import {ItemContainer, ItemInfo, ItemTitle} from './styles';

const DetailsItem: FC<DetailsItemProps> = ({title, info}) => {
  return (
    <ItemContainer>
      <ItemTitle>{title}</ItemTitle>
      <ItemInfo>{info}</ItemInfo>
    </ItemContainer>
  );
};

export default DetailsItem;
