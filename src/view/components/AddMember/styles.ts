import styled from 'styled-components/native';

export const StyledContainer = styled.View`
  width: 35px;
  height: 35px;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: #bfb393;
  border-radius: 30px;
`;
