import React from 'react';
import {StyledContainer} from './styles';
import Plus from '../../icons/Plus';

const AddMember = () => {
  return (
    <StyledContainer>
      <Plus fillPath="#fff" />
    </StyledContainer>
  );
};

export default AddMember;
