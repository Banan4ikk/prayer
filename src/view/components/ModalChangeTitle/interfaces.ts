import React, {SetStateAction} from 'react';

export interface IModalProps {
  setIsVisibleModal: React.Dispatch<SetStateAction<boolean>>;
  onSubmitEditing: (title: string) => void;
}
export interface ISubmitValue {
  value: string;
}
