import styled from 'styled-components/native';

export const ArrowContainer = styled.View`
  position: absolute;
  top: 10px;
  left: 10px;
`;

export const Container = styled.View`
  position: absolute;
  top: 0;
  left: 0;
  width: 110%;
  height: 110%;
  background: rgba(0, 0, 0, 0.7);
  z-index: 9999;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const StyledInput = styled.TextInput`
  border-radius: 30px;
  width: 300px;
  border: 2px solid #e5e5e5;
  padding: 15px;
  font-size: 18px;
  color: #c8c8c8;
  margin-top: 20px;
  background-color: #fff;
`;
