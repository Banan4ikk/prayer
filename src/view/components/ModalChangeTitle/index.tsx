import React, {FC} from 'react';
import {Field, Form} from 'react-final-form';
import {ArrowContainer, Container, StyledInput} from './styles';
import {IModalProps, ISubmitValue} from './interfaces';
import Back from '../../icons/Back';
import {TouchableOpacity} from 'react-native';

const ModalChangeTitle: FC<IModalProps> = ({
  setIsVisibleModal,
  onSubmitEditing,
}) => {
  const onSubmit = ({value}: ISubmitValue) => {
    onSubmitEditing(value);
    setIsVisibleModal(false);
  };

  const onCancel = () => {
    setIsVisibleModal(false);
  };

  return (
    <Form
      onSubmit={onSubmit}
      render={({handleSubmit}) => (
        <Container>
          <ArrowContainer>
            <TouchableOpacity onPress={onCancel}>
              <Back />
            </TouchableOpacity>
          </ArrowContainer>
          <Field name="value" type="text" placeholder="Input new title...">
            {({input, placeholder}) => (
              <StyledInput
                onChange={input.onChange}
                placeholder={placeholder}
                value={input.value}
                onSubmitEditing={handleSubmit}
              />
            )}
          </Field>
        </Container>
      )}
    />
  );
};

export default ModalChangeTitle;
