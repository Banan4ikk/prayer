import React, {FC, useState} from 'react';
import {TouchableOpacity, View} from 'react-native';
import {ICommentProps} from './interfaces';
import {
  CommentContainer,
  StyledComment,
  StyledContainer,
  StyledName,
} from './styles';
import User from '../../icons/User';
import DeleteButton from '../DeleteButton';
import {SwipeRow} from 'react-native-swipe-list-view';
import {useDispatch} from 'react-redux';
import {actions} from '../../../redux/ducks';

const Comment: FC<ICommentProps> = ({
  comment,
  name,
  id,
  setCommentId,
  setIsVisibleModal,
}) => {
  const [isVisibleButton, setIsVisibleButton] = useState(false);
  const dispatch = useDispatch();

  const onDelete = () => {
    dispatch({type: actions.comments.deleteComment.type, payload: id});
  };

  return (
    <SwipeRow
      rightOpenValue={-100}
      onRowOpen={() => setIsVisibleButton(true)}
      onRowClose={() => setIsVisibleButton(false)}
      disableRightSwipe={true}>
      {isVisibleButton ? (
        <DeleteButton text="Delete" onPress={onDelete} />
      ) : (
        <View />
      )}
      <StyledContainer>
        <CommentContainer>
          <User />
        </CommentContainer>
        <TouchableOpacity
          onLongPress={() => {
            setIsVisibleModal(true);
            setCommentId(id);
          }}>
          <View>
            <StyledName>{name}</StyledName>
            <StyledComment>{comment}</StyledComment>
          </View>
        </TouchableOpacity>
      </StyledContainer>
    </SwipeRow>
  );
};

export default Comment;
