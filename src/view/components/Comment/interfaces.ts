import React, {SetStateAction} from 'react';

export interface ICommentProps {
  id: number;
  name: string;
  comment: string;
  setCommentId: React.Dispatch<SetStateAction<number>>;
  setIsVisibleModal: React.Dispatch<SetStateAction<boolean>>;
}
