import styled from 'styled-components/native';

export const StyledContainer = styled.View`
  display: flex;
  flex-direction: row;
  height: 90px;
  border: 1px solid #e5e5e5;
  align-items: center;
  padding: 15px;
`;

export const CommentContainer = styled.View`
  width: 50px;
  height: 50px;
`;

export const StyledImage = styled.Image`
  border-radius: 30px;
`;

export const StyledName = styled.Text`
  font-size: 19px;
  font-weight: bold;
  color: #514d47;
`;

export const StyledComment = styled.Text`
  color: #514d47;
  font-size: 19px;
`;
