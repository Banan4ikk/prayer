import styled from 'styled-components/native';

export const StyledContainer = styled.View`
  margin-top: 15px;
  margin-right: 30px;
  width: 100%;
  display: flex;
  align-items: center;
`;

export const AdvancedPrayersText = styled.Text`
  color: #fff;
  text-transform: uppercase;
  font-weight: bold;
`;

export const AdvancedPrayersContainer = styled.View`
  width: 220px;
  height: 30px;
  background-color: #bfb393;
  border-radius: 15px;
  box-shadow: 0 2px rgba(66, 78, 117, 0.1);
  display: flex;
  justify-content: center;
  align-items: center;
`;
