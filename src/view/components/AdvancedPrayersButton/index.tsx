import React, {FC} from 'react';
import {
  AdvancedPrayersContainer,
  AdvancedPrayersText,
  StyledContainer,
} from './styles';
import {IButtonProps} from './interfaces';

const AdvancedPrayersButton: FC<IButtonProps> = ({title}) => {
  return (
    <StyledContainer>
      <AdvancedPrayersContainer>
        <AdvancedPrayersText>{title}</AdvancedPrayersText>
      </AdvancedPrayersContainer>
    </StyledContainer>
  );
};

export default AdvancedPrayersButton;
