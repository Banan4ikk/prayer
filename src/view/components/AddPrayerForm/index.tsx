import React, {FC} from 'react';
import {StyledContainer, StyledInput} from './styles';
import Plus from '../../icons/Plus';
import {IAddPrayer, IAddPrayerFormProps} from './interfaces';
import {Field, Form} from 'react-final-form';
import {useDispatch} from 'react-redux';
import {actions} from '../../../redux/ducks';

const AddPrayerForm: FC<IAddPrayerFormProps> = ({columnId}) => {
  const dispatch = useDispatch();

  const onSubmit = (prayer: IAddPrayer) => {
    const payload = {
      title: prayer.title,
      description: '',
      checked: false,
      columnId,
    };

    dispatch({type: actions.prayers.addPrayer.type, payload});
  };

  return (
    <Form
      onSubmit={onSubmit}
      render={({handleSubmit, form}) => (
        <StyledContainer>
          <Plus />
          <Field name="title" placeholder="Add a prayer...">
            {({input, placeholder}) => {
              return (
                <StyledInput
                  onChange={input.onChange}
                  placeholder={placeholder}
                  value={input.value}
                  onSubmitEditing={() => {
                    handleSubmit();
                    form.reset();
                  }}
                />
              );
            }}
          </Field>
        </StyledContainer>
      )}
    />
  );
};

export default AddPrayerForm;
