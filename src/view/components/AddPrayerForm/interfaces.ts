export interface IAddPrayerFormProps {
  columnId: number;
}
export interface IAddPrayer {
  title: string;
}
