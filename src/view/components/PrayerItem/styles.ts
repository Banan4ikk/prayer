import styled from 'styled-components/native';

export const PrayerContainer = styled.View`
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-direction: row;
  border-bottom: 1px solid #e5e5e5;
  padding-bottom: 10px;
  background-color: #fff;
  height: 70px;
`;

export const PrayersTitle = styled.View`
  display: flex;
  align-items: center;
  justify-content: flex-start;
  flex-direction: row;
`;

export const StateStyles = styled.Image`
  margin-right: 10px;
`;

export const CheckedTextStyles = styled.Text`
  font-size: 20px;
  text-decoration: line-through;
`;

export const StyledText = styled.Text`
  font-size: 20px;
`;

export const IconContainer = styled.View`
  display: flex;
  justify-content: space-between;
  flex-direction: row;
  width: 110px;
`;

export const PrayersIcon = styled.View`
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-direction: row;
`;

export const IconsStyles = styled.View`
  margin-right: 7px;
`;
