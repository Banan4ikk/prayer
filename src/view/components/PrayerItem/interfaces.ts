import React, {SetStateAction} from 'react';

export interface IPrayerItemProps {
  id: number;
  title: string;
  usersCount: number;
  prayers: number;
  setIsVisibleModal: React.Dispatch<SetStateAction<boolean>>;
}
