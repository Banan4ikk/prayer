import React, {FC, useState} from 'react';
import {Text, TouchableOpacity} from 'react-native';
import {IPrayerItemProps} from './interfaces';
import {
  CheckedTextStyles,
  IconContainer,
  IconsStyles,
  PrayerContainer,
  PrayersIcon,
  PrayersTitle,
  StyledText,
} from './styles';
import CheckBox from '@react-native-community/checkbox';
import {SwipeRow} from 'react-native-swipe-list-view';
import {useNavigation} from '@react-navigation/native';
import {NavigationPrayerItemProps} from './types';
import Prayers from '../../icons/Prayers';
import User from '../../icons/User';
import State from '../../icons/State';
import {PRAYERS_DETAILS_SCREEN_ROUTE} from '../../../navigation/UserNavigation/routes';
import {useDispatch} from 'react-redux';
import {actions} from '../../../redux/ducks';
import DeleteButton from '../DeleteButton';

const PrayerItem: FC<IPrayerItemProps> = ({
  title,
  prayers,
  usersCount,
  id,
  setIsVisibleModal,
}) => {
  const [isChecked, setIsChecked] = useState(false);

  const navigation = useNavigation<NavigationPrayerItemProps>();
  const dispatch = useDispatch();

  const onPressCheckbox = () => {
    setIsChecked(!isChecked);
  };

  const onPressItem = () => {
    navigation.navigate(PRAYERS_DETAILS_SCREEN_ROUTE, {prayerId: id});
  };

  const onLongPress = () => {
    navigation.setParams({prayerId: id});
    setIsVisibleModal(true);
  };

  const onDelete = () => {
    dispatch({type: actions.prayers.deletePrayer.type, payload: id});
  };

  return (
    <SwipeRow rightOpenValue={-100} disableRightSwipe={true}>
      <DeleteButton text="Delete" onPress={onDelete} />
      <TouchableOpacity
        activeOpacity={1}
        onPress={onPressItem}
        onLongPress={onLongPress}>
        <PrayerContainer>
          <PrayersTitle>
            <State />
            <CheckBox value={isChecked} onChange={onPressCheckbox} />
            {isChecked ? (
              <CheckedTextStyles>{title}</CheckedTextStyles>
            ) : (
              <StyledText>{title}</StyledText>
            )}
          </PrayersTitle>
          <IconContainer>
            <PrayersIcon>
              <IconsStyles>
                <User />
              </IconsStyles>
              <Text>{usersCount}</Text>
            </PrayersIcon>
            <PrayersIcon>
              <IconsStyles>
                <Prayers />
              </IconsStyles>
              <Text>{prayers}</Text>
            </PrayersIcon>
          </IconContainer>
        </PrayerContainer>
      </TouchableOpacity>
    </SwipeRow>
  );
};

export default PrayerItem;
