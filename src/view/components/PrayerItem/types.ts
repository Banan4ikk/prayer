import {RouteProp} from '@react-navigation/native';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import {RootStackParamList} from '../../../types';

export type NavigationPrayerItemProps = NativeStackNavigationProp<
  RootStackParamList,
  'PrayerDetails'
>;

export type RoutePrayerItemProps = RouteProp<
  RootStackParamList,
  'PrayerDetails'
>;
