export interface IDeleteButtonProps {
  text: string;
  onPress: () => void;
}
