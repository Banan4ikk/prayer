import React, {FC} from 'react';
import {TouchableOpacity} from 'react-native';
import {DeleteContainer, DeleteText} from './styles';
import {IDeleteButtonProps} from './intrefaces';

const DeleteButton: FC<IDeleteButtonProps> = ({text, onPress}) => {
  return (
    <DeleteContainer>
      <TouchableOpacity onPress={onPress}>
        <DeleteText>{text}</DeleteText>
      </TouchableOpacity>
    </DeleteContainer>
  );
};

export default DeleteButton;
