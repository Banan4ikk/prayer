import styled from 'styled-components/native';

export const DeleteContainer = styled.View`
  display: flex;
  align-items: center;
  justify-content: center;
  position: absolute;
  right: 0;
  height: 70px;
  width: 90px;
  background-color: #ac5253;
  margin-left: 30px;
`;

export const DeleteText = styled.Text`
  color: #fff;
  font-size: 18px;
`;
