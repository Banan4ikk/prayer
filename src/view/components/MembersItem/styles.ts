import {StyleSheet} from 'react-native';
import styled from 'styled-components/native';

export const styles = StyleSheet.create({
  itemContainer: {
    width: 40,
    height: 40,
  },
  imgStyles: {
    borderRadius: 30,
  },
});

export const ItemContainer = styled.View`
  width: 40px;
  height: 40px;
`;
export const StyledImage = styled.Image`
  border-radius: 30px;
`;
