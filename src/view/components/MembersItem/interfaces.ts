import {ImageSourcePropType} from 'react-native';

export interface MembersItemProps {
  img: ImageSourcePropType;
}
