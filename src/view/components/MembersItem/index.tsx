import React, {FC} from 'react';
import {MembersItemProps} from './interfaces';
import {ItemContainer, StyledImage} from './styles';

const DetailsItem: FC<MembersItemProps> = ({img}) => {
  return (
    <ItemContainer>
      <StyledImage source={img} />
    </ItemContainer>
  );
};

export default DetailsItem;
