import styled from 'styled-components/native';

export const MenuItem = styled.View`
  height: 55px;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
`;
export const MenuText = styled.Text`
  text-transform: uppercase;
  font-size: 18px;
  font-weight: bold;
  color: #c8c8c8;
  width: 100%;
`;
export const ActiveMenuText = styled.Text`
  text-transform: uppercase;
  font-size: 18px;
  font-weight: bold;
  color: #72a8bc;
`;
export const ActiveItemMenu = styled.View`
  width: 100%;
  height: 60px;
  display: flex;
  justify-content: center;
  align-items: center;
`;
export const CountContainer = styled.View`
  right: -25px;
  top: 2px;
  position: absolute;
  width: 20px;
  height: 20px;
  border-radius: 20px;
  background-color: #ac5253;
  display: flex;
  justify-content: center;
  align-items: center;
  transform: translateY(15px);
`;
export const CountText = styled.Text`
  font-weight: bold;
  color: #fff;
`;
