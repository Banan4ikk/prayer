import React, {FC} from 'react';
import {View} from 'react-native';
import {ITabBarProps} from './interfaces';
import {
  ActiveItemMenu,
  ActiveMenuText,
  CountContainer,
  CountText,
  MenuItem,
  MenuText,
} from './styles';

const TabBarSubscribeHeader: FC<ITabBarProps> = ({
  title,
  focused,
  isSubscribe = false,
}) => {
  return (
    <View>
      {focused ? (
        <ActiveItemMenu>
          <ActiveMenuText>{title}</ActiveMenuText>
          {isSubscribe ? (
            <CountContainer>
              <CountText>3</CountText>
            </CountContainer>
          ) : null}
        </ActiveItemMenu>
      ) : (
        <MenuItem>
          <MenuText>{title}</MenuText>
          {isSubscribe ? (
            <CountContainer>
              <CountText>3</CountText>
            </CountContainer>
          ) : null}
        </MenuItem>
      )}
    </View>
  );
};

export default TabBarSubscribeHeader;
