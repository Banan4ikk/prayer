export interface ITabBarProps {
  title: string;
  focused: boolean;
  isSubscribe?: boolean;
}
