import {useSelector} from 'react-redux';
import {selectors} from '../redux/ducks';
import UserNavigation from './UserNavigation/Navigator';
import AuthNavigation from './AuthNavigation/Navigator';
import React from 'react';

const StackApp = () => {
  const isToken = useSelector(selectors.auth.getToken);
  return isToken ? <UserNavigation /> : <AuthNavigation />;
};

export default StackApp;
