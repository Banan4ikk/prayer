import React, {useState} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import PrayerDetails from './screens/PrayerDetails';
import AllPrayersScreen from './screens/AllPrayersScreen';
import Desk from './screens/Desk';
import Header from './screens/Header';
import PrayerDetailsHeader from './screens/PrayerDetailsHeader';
import Plus from '../../view/icons/Plus';
import Settings from '../../view/icons/Settings';
import {
  HOME_SCREEN_ROUTE,
  PRAYER_SCREEN_ROUTE,
  PRAYERS_DETAILS_SCREEN_ROUTE,
} from './routes';

const UserNavigation = () => {
  const [columnTitle, setColumnTitle] = useState('');

  const Stack = createNativeStackNavigator();
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name={HOME_SCREEN_ROUTE}
          options={{
            header: () => <Header title="To do" Icon={Plus} />,
          }}>
          {() => <Desk setColumnTitle={setColumnTitle} />}
        </Stack.Screen>
        <Stack.Screen
          name={PRAYER_SCREEN_ROUTE}
          component={AllPrayersScreen}
          options={{
            header: () => <Header title={columnTitle} Icon={Settings} />,
          }}
        />
        <Stack.Screen
          name={PRAYERS_DETAILS_SCREEN_ROUTE}
          component={PrayerDetails}
          options={{header: PrayerDetailsHeader}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default UserNavigation;
