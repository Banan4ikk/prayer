import React, {FC} from 'react';
import {TouchableOpacity} from 'react-native';
import {StyledContainer, StyledText} from './styles';
import {ICardProps} from './interfaces';
import {useNavigation} from '@react-navigation/native';
import {NavigationHeaderProps} from './types';
import {PRAYER_SCREEN_ROUTE} from '../../routes';
import {SwipeRow} from 'react-native-swipe-list-view';
import DeleteButton from '../../../../view/components/DeleteButton';
import {useDispatch} from 'react-redux';
import {actions} from '../../../../redux/ducks';

const DefaultCard: FC<ICardProps> = ({
  title,
  setColumnTitle,
  columnId,
  setIsVisibleModal,
}) => {
  const navigation = useNavigation<NavigationHeaderProps>();
  const dispatch = useDispatch();

  const onClickTitle = () => {
    navigation.navigate(PRAYER_SCREEN_ROUTE, {columnId});
    setColumnTitle(title);
  };

  const onDelete = () => {
    dispatch({type: actions.columns.deleteColumn.type, payload: columnId});
  };

  const onLongPress = () => {
    setIsVisibleModal(true);
    navigation.setParams({columnId});
  };

  return (
    <SwipeRow rightOpenValue={-100} disableRightSwipe={true}>
      <DeleteButton text="Delete" onPress={onDelete} />
      <StyledContainer>
        <TouchableOpacity onPress={onClickTitle} onLongPress={onLongPress}>
          <StyledText>{title}</StyledText>
        </TouchableOpacity>
      </StyledContainer>
    </SwipeRow>
  );
};

export default DefaultCard;
