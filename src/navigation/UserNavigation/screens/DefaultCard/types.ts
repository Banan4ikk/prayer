import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import {RootStackParamList} from '../../../../types';
import {RouteProp} from '@react-navigation/native';

export type NavigationHeaderProps = NativeStackNavigationProp<
  RootStackParamList,
  'PrayersScreen'
>;

export type PrayersScreenScreenProps = RouteProp<
  RootStackParamList,
  'PrayersScreen'
>;
