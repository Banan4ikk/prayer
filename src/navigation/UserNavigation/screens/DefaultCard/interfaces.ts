import React, {Dispatch, SetStateAction} from 'react';

export interface ICardProps {
  title: string;
  setColumnTitle: Dispatch<SetStateAction<string>>;
  columnId: number;
  setIsVisibleModal: React.Dispatch<SetStateAction<boolean>>;
}
