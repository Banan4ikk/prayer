import styled from 'styled-components/native';

export const StyledContainer = styled.View`
  width: 101%;
  height: 70px;
  margin-bottom: 10px;
  border-radius: 5px;
  border: 1px solid #e5e5e5;
  display: flex;
  padding: 15px;
  background-color: #fff;
`;

export const StyledText = styled.Text`
  font-size: 20px;
  color: #514d47;
  font-weight: bold;
`;
