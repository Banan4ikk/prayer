import React, {useState} from 'react';
import {TouchableOpacity} from 'react-native';
import {StyledDesk} from '../PrayerDesk/styles';
import PrayerItem from '../../../../view/components/PrayerItem';
import AdvancedPrayersButton from '../../../../view/components/AdvancedPrayersButton';

const SubscribedDesk = () => {
  const [isShowAdvanced, setIsShowAdvanced] = useState(false);

  const onShowAdvanced = () => {
    setIsShowAdvanced(!isShowAdvanced);
  };

  const title = isShowAdvanced
    ? 'hide Answered Prayers'
    : 'Show Answered Prayers';

  const cards = [
    {
      id: 0,
      title: 'item 1',
      usersCount: 10,
      prayers: 124,
    },
    {
      id: 1,
      title: 'item 2',
      usersCount: 18,
      prayers: 101,
    },
  ];

  return (
    <StyledDesk>
      {cards?.map(item => (
        <PrayerItem
          key={item.id}
          title={item.title}
          usersCount={item.usersCount}
          prayers={item.prayers}
        />
      ))}
      <TouchableOpacity onPress={onShowAdvanced}>
        <AdvancedPrayersButton title={title} />
      </TouchableOpacity>
      {isShowAdvanced ? (
        <PrayerItem title="Prayer Item one ..." usersCount={3} prayers={123} />
      ) : null}
    </StyledDesk>
  );
};

export default SubscribedDesk;
