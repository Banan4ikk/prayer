import React from 'react';
import {TouchableOpacity} from 'react-native';
import {
  HeaderBack,
  HeaderContainer,
  HeaderText,
  HeaderTextContainer,
} from './styles';
import {useNavigation} from '@react-navigation/native';
import {NavigationBackProps} from './types';
import Prayers from '../../../../view/icons/Prayers';
import Back from '../../../../view/icons/Back';

const PrayerDetailsHeader = () => {
  const navigation = useNavigation<NavigationBackProps>();

  const onPressBack = () => {
    navigation.goBack();
  };

  return (
    <HeaderContainer>
      <HeaderBack>
        <TouchableOpacity onPress={onPressBack}>
          <Back />
        </TouchableOpacity>
        <Prayers fillPath={'#fff'} />
      </HeaderBack>
      <HeaderTextContainer>
        <HeaderText>
          Prayer item two which is for my family to love God whole heartedly.
        </HeaderText>
      </HeaderTextContainer>
    </HeaderContainer>
  );
};

export default PrayerDetailsHeader;
