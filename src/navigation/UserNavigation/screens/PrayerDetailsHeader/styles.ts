import styled from 'styled-components/native';

export const HeaderContainer = styled.View`
  width: 100%;
  height: 120px;
  background-color: #bfb393;
  padding: 15px;
`;
export const HeaderBack = styled.View`
  display: flex;
  justify-content: space-between;
  flex-direction: row;
  margin-bottom: 10px;
`;
export const HeaderTextContainer = styled.View`
  width: 100%;
`;
export const HeaderText = styled.Text`
  color: #fff;
  font-size: 20px;
`;
