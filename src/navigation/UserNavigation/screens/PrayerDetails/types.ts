import {RouteProp} from '@react-navigation/native';
import {RootStackParamList} from '../../../../types';

export type PrayerDetailsProps = RouteProp<RootStackParamList, 'PrayerDetails'>;
