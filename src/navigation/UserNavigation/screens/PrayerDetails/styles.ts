import styled from 'styled-components/native';

export const LastPrayedContainer = styled.View`
  width: 100%;
  padding: 15px;
  display: flex;
  flex-direction: row;
  align-items: center;
`;
export const LastPrayedText = styled.Text`
  margin-left: 7px;
  color: #514d47;
  font-size: 20px;
`;
export const DetailsContainer = styled.View`
  display: flex;
  flex-direction: row;
`;
export const MembersContainer = styled.View`
  padding: 15px;
`;
export const MembersTitle = styled.Text`
  color: #72a8bc;
  font-size: 17px;
  text-transform: uppercase;
  font-weight: bold;
  margin-bottom: 15px;
`;
export const MembersAvatarsContainer = styled.View`
  display: flex;
  flex-direction: row;
`;
