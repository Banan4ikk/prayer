import React, {useEffect, useState} from 'react';
import {ScrollView, View} from 'react-native';
import {
  DetailsContainer,
  LastPrayedContainer,
  LastPrayedText,
  MembersAvatarsContainer,
  MembersContainer,
  MembersTitle,
} from './styles';
import DetailsItem from '../../../../view/components/DetailsItem';
import MembersItem from '../../../../view/components/MembersItem';
import Comment from '../../../../view/components/Comment';
import AddCommentForm from '../../../../view/components/AddCommentForm';
import AddMember from '../../../../view/components/AddMember';
import State from '../../../../view/icons/State';
import {useRoute} from '@react-navigation/native';
import {PrayerDetailsProps} from './types';
import {useDispatch, useSelector} from 'react-redux';
import {actions, selectors} from '../../../../redux/ducks';
import ModalChangeTitle from '../../../../view/components/ModalChangeTitle';
import {IChangeComment} from '../../../../redux/comments/types';

const PrayerDetails = () => {
  const route = useRoute<PrayerDetailsProps>();
  const prayerId = route.params?.prayerId;
  const dispatch = useDispatch();
  const [isVisibleModal, setIsVisibleModal] = useState(false);
  const [commentId, setCommentId] = useState(-1);

  useEffect(() => {
    dispatch({type: actions.comments.getComments.type});
  }, []);

  const comments = useSelector(
    selectors.comments.getCommentsByPrayerId(prayerId),
  );
  const userName = useSelector(selectors.auth.getUserName);

  const onSubmitEditing = (value: string) => {
    const payload: IChangeComment = {
      commentId,
      body: value,
    };

    dispatch({type: actions.comments.changeComment.type, payload});
  };

  return (
    <ScrollView>
      <View>
        <View>
          <LastPrayedContainer>
            <State />
            <LastPrayedText>Last prayed 8 min ago</LastPrayedText>
          </LastPrayedContainer>
          <DetailsContainer>
            <DetailsItem title="July 25 2017" info="Date Added" />
            <DetailsItem title="123" info="Times Prayed Total" />
          </DetailsContainer>
          <DetailsContainer>
            <DetailsItem title="July 25 2017" info="Date Added" />
            <DetailsItem title="123" info="Times Prayed Total" />
          </DetailsContainer>
          <MembersContainer>
            <MembersTitle>members</MembersTitle>
            <MembersAvatarsContainer>
              <MembersItem
                img={require('../../../../../public/img/eeww.png')}
              />
              <AddMember />
            </MembersAvatarsContainer>
          </MembersContainer>
          <View>
            <MembersContainer>
              <MembersTitle>comments</MembersTitle>
            </MembersContainer>
            {comments.map(item => (
              <Comment
                key={item.id}
                id={item.id}
                name={userName}
                comment={item.body}
                setIsVisibleModal={setIsVisibleModal}
                setCommentId={setCommentId}
              />
            ))}
          </View>
          <AddCommentForm prayerId={prayerId} />
          {isVisibleModal && (
            <ModalChangeTitle
              onSubmitEditing={onSubmitEditing}
              setIsVisibleModal={setIsVisibleModal}
            />
          )}
        </View>
      </View>
    </ScrollView>
  );
};

export default PrayerDetails;
