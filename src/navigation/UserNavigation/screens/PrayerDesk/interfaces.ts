export interface IPrayerProps {
  columnId: number;
}

interface IChangePrayerData {
  title: string | undefined;
  description: string | undefined;
}

export interface IChangePrayer {
  prayerId: number;
  requestData: IChangePrayerData;
}
