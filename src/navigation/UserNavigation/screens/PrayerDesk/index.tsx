import React, {useEffect, useState} from 'react';
import {TouchableOpacity} from 'react-native';
import {StyledDesk} from './styles';
import PrayerItem from '../../../../view/components/PrayerItem';
import AddPrayerForm from '../../../../view/components/AddPrayerForm';
import AdvancedPrayersButton from '../../../../view/components/AdvancedPrayersButton';
import {useDispatch, useSelector} from 'react-redux';
import {actions, selectors} from '../../../../redux/ducks';
import {useRoute} from '@react-navigation/native';
import {PrayersScreenScreenProps} from '../DefaultCard/types';
import ModalChangeTitle from '../../../../view/components/ModalChangeTitle';
import {RoutePrayerItemProps} from '../../../../view/components/PrayerItem/types';
import {IChangePrayer} from './interfaces';

const PrayerDesk = () => {
  const [isShowAdvanced, setIsShowAdvanced] = useState(false);
  const [isVisibleModal, setIsVisibleModal] = useState(false);
  const dispatch = useDispatch();
  const columnRoute = useRoute<PrayersScreenScreenProps>();
  const prayerRoute = useRoute<RoutePrayerItemProps>();
  const columnId = columnRoute.params?.columnId;
  const prayerId = prayerRoute.params?.prayerId;
  const prayerData = useSelector(selectors.prayers.getPrayerDataById(prayerId));
  const prayers = useSelector(selectors.prayers.getPrayersByColumnId(columnId));

  useEffect(() => {
    dispatch({type: actions.prayers.getPrayers.type});
  }, []);

  const onShowAdvanced = () => {
    setIsShowAdvanced(!isShowAdvanced);
  };

  const onSubmitEditing = (title: string) => {
    const {description} = prayerData;
    const payload: IChangePrayer = {
      prayerId,
      requestData: {
        title,
        description,
      },
    };
    dispatch({type: actions.prayers.changePrayer.type, payload});
  };

  const title = isShowAdvanced
    ? 'hide Answered Prayers'
    : 'Show Answered Prayers';

  return (
    <StyledDesk>
      <AddPrayerForm columnId={columnId} />
      {prayers.map(item => (
        <PrayerItem
          key={item.id}
          id={item.id}
          title={item.title}
          usersCount={0}
          prayers={0}
          setIsVisibleModal={setIsVisibleModal}
        />
      ))}
      <TouchableOpacity onPress={onShowAdvanced}>
        <AdvancedPrayersButton title={title} />
      </TouchableOpacity>
      {isShowAdvanced ? (
        <PrayerItem
          title="Prayer Item one ..."
          usersCount={3}
          prayers={123}
          id={10}
          setIsVisibleModal={setIsVisibleModal}
        />
      ) : null}
      {isVisibleModal && (
        <ModalChangeTitle
          setIsVisibleModal={setIsVisibleModal}
          onSubmitEditing={onSubmitEditing}
        />
      )}
    </StyledDesk>
  );
};

export default PrayerDesk;
