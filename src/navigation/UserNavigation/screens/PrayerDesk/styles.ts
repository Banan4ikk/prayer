import styled from 'styled-components/native';

export const StyledDesk = styled.View`
  display: flex;
  flex-direction: column;
  height: 100%;
  width: 100%;
  background-color: #fff;
  padding: 15px;
`;
