import React from 'react';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import PrayerDesk from '../PrayerDesk';
import SubscribedDesk from '../SubscribedDesk';
import TabBarPrayersHeader from '../../../../view/components/TabBarPrayersHeader';
import {SUBSCRIBED_SCREEN_ROUTE, TAB_PRAYER_SCREEN_ROUTE} from '../../routes';
import {useRoute} from '@react-navigation/native';
import {PrayersScreenScreenProps} from '../DefaultCard/types';

const AllPrayersScreen = () => {
  const Tab = createMaterialTopTabNavigator();

  const currentRoute = useRoute<PrayersScreenScreenProps>();

  return (
    <Tab.Navigator
      screenOptions={({route}) => ({
        tabBarLabel: ({focused}) => {
          if (route.name === TAB_PRAYER_SCREEN_ROUTE) {
            return <TabBarPrayersHeader title="my prayers" focused={focused} />;
          } else if (route.name === SUBSCRIBED_SCREEN_ROUTE) {
            return (
              <TabBarPrayersHeader
                title="subscribed"
                focused={focused}
                isSubscribe={true}
              />
            );
          }
        },
        tabBarActiveTintColor: '#72A8BC',
        tabBarInactiveTintColor: '#C8C8C8',
        swipeEnabled: false,
      })}>
      <Tab.Screen
        name={TAB_PRAYER_SCREEN_ROUTE}
        component={PrayerDesk}
        initialParams={currentRoute.params}
      />
      <Tab.Screen name={SUBSCRIBED_SCREEN_ROUTE} component={SubscribedDesk} />
    </Tab.Navigator>
  );
};

export default AllPrayersScreen;
