import React, {FC} from 'react';
import {HeaderStyled, StyledContainer, StyledImage, StyledText} from './styles';
import {IHeaderProps} from './interfaces';
import {TouchableOpacity} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {actions, selectors} from '../../../../redux/ducks';
import {IAddColumn} from '../../../../redux/column/types';

const Header: FC<IHeaderProps> = ({title, Icon}) => {
  const dispatch = useDispatch();
  const userId = useSelector(selectors.auth.getUserId);

  const onAdd = () => {
    const payload: IAddColumn = {
      title: 'Default Title',
      description: null,
      userId,
    };
    dispatch({type: actions.columns.addColumn.type, payload});
  };

  return (
    <StyledContainer>
      <HeaderStyled>
        <StyledText>{title}</StyledText>
        <StyledImage>
          <TouchableOpacity onPress={onAdd}>
            <Icon />
          </TouchableOpacity>
        </StyledImage>
      </HeaderStyled>
    </StyledContainer>
  );
};

export default Header;
