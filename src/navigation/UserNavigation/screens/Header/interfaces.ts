import {SvgProps} from 'react-native-svg';

export interface IHeaderProps {
  title: string;
  Icon: (props: SvgProps) => JSX.Element;
}
