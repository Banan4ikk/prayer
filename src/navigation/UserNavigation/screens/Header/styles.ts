import styled from 'styled-components/native';

export const StyledContainer = styled.View`
  height: 70px;
  display: flex;
  flex-direction: row;
`;
export const HeaderStyled = styled.View`
  width: 100%;
  height: 75px;
  display: flex;
  justify-content: center;
  align-items: center;
`;
export const StyledText = styled.Text`
  font-size: 25px;
  color: #514d47;
  font-weight: bold;
`;
export const StyledImage = styled.View`
  position: absolute;
  right: 15px;
  height: 20px;
  width: 20px;
  z-index: 100000;
`;
