import styled from 'styled-components/native';

export const DeskContainer = styled.View`
  height: 100%;
  width: 100%;
  background-color: #fff;
  padding: 15px;
`;
