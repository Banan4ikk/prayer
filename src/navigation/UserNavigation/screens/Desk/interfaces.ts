import {Dispatch, SetStateAction} from 'react';

export interface IDeskProps {
  setColumnTitle: Dispatch<SetStateAction<string>>;
}

export interface IColumnChangeData {
  title: string;
  description: string | undefined;
}

export interface IColumnChange {
  columnId: number;
  requestData: IColumnChangeData;
}
