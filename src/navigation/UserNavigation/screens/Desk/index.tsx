import React, {FC, useEffect, useState} from 'react';
import DefaultCard from '../DefaultCard';
import {DeskContainer} from './styles';
import {useDispatch, useSelector} from 'react-redux';
import {actions, selectors} from '../../../../redux/ducks';
import {IColumnChange, IDeskProps} from './interfaces';
import ModalChangeTitle from '../../../../view/components/ModalChangeTitle';
import {useRoute} from '@react-navigation/native';
import {PrayersScreenScreenProps} from '../DefaultCard/types';

const Desk: FC<IDeskProps> = ({setColumnTitle}) => {
  const dispatch = useDispatch();

  const columns = useSelector(selectors.columns.getColumns);
  const route = useRoute<PrayersScreenScreenProps>();
  const columnId = route.params?.columnId;
  const description = useSelector(
    selectors.columns.getColumnDescById(columnId),
  );

  const onSubmitChangeTitle = (title: string) => {
    const payload: IColumnChange = {
      columnId,
      requestData: {
        title,
        description,
      },
    };

    dispatch({type: actions.columns.changeTitle.type, payload});
  };

  const [isVisibleModal, setIsVisibleModal] = useState(false);

  useEffect(() => {
    dispatch({type: actions.columns.getColumnData.type});
  }, []);

  return (
    <DeskContainer>
      {columns.map(column => {
        return (
          <DefaultCard
            key={column.id}
            title={column.title}
            setColumnTitle={setColumnTitle}
            columnId={column.id}
            setIsVisibleModal={setIsVisibleModal}
          />
        );
      })}
      {isVisibleModal && (
        <ModalChangeTitle
          setIsVisibleModal={setIsVisibleModal}
          onSubmitEditing={onSubmitChangeTitle}
        />
      )}
    </DeskContainer>
  );
};

export default Desk;
