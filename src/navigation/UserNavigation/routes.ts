export const TAB_PRAYER_SCREEN_ROUTE = 'TabPrayersScreen';
export const SUBSCRIBED_SCREEN_ROUTE = 'SubscribeScreen';
export const PRAYER_SCREEN_ROUTE = 'PrayersScreen';
export const HOME_SCREEN_ROUTE = 'HomeScreen';
export const PRAYERS_DETAILS_SCREEN_ROUTE = 'PrayerDetails';
