export const AUTH_ROUTE_TYPE = 'authorization';
export const REGISTER_ROUTE_TYPE = 'registration';
export const AUTH_ROUTE = 'auth';
export const REGISTER_ROUTE = 'register';
