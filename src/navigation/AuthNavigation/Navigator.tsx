import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {AUTH_ROUTE, REGISTER_ROUTE} from './routes';
import AuthForm from './screens/AuthForm';
import RegisterFrom from './screens/RegisterForm';

const AuthNavigation = () => {
  const Stack = createNativeStackNavigator();

  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerShown: false}}>
        <Stack.Screen name={AUTH_ROUTE} component={AuthForm} />
        <Stack.Screen name={REGISTER_ROUTE} component={RegisterFrom} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default AuthNavigation;
