import React from 'react';
import {Field, Form} from 'react-final-form';
import {TouchableOpacity} from 'react-native';
import {
  ButtonContainer,
  SignUpText,
  StyledButton,
  StyledButtonText,
  StyledForm,
  StyledInput,
} from './styles';
import {AUTH_ROUTE} from '../../routes';
import {useNavigation} from '@react-navigation/native';
import {NavigationAuthProps} from '../AuthForm/types';
import {useDispatch} from 'react-redux';
import {actions} from '../../../../redux/ducks';
import {IRegisterSubmitValue} from './interfaces';

const RegisterFrom = () => {
  const navigation = useNavigation<NavigationAuthProps>();
  const dispatch = useDispatch();

  const onSubmit = (payload: IRegisterSubmitValue) => {
    dispatch({type: actions.auth.signUp.type, payload});
  };

  const onClickText = () => {
    navigation.navigate(AUTH_ROUTE);
  };

  return (
    <Form
      onSubmit={onSubmit}
      render={({handleSubmit}) => (
        <StyledForm>
          <Field name="email" type="email" placeholder="Email">
            {({input, placeholder}) => {
              return (
                <StyledInput
                  onChange={input.onChange}
                  placeholder={placeholder}
                  value={input.value}
                  // onSubmitEditing={handleSubmit}
                />
              );
            }}
          </Field>
          <Field name="name" type="text" placeholder="Name">
            {({input, placeholder}) => {
              return (
                <StyledInput
                  onChange={input.onChange}
                  placeholder={placeholder}
                  value={input.value}
                />
              );
            }}
          </Field>
          <Field name="password" type="password" placeholder="Password">
            {({input, placeholder}) => {
              return (
                <StyledInput
                  onChange={input.onChange}
                  placeholder={placeholder}
                  value={input.value}
                />
              );
            }}
          </Field>
          <ButtonContainer>
            <TouchableOpacity onPress={onClickText}>
              <SignUpText>{'Sign In'}</SignUpText>
            </TouchableOpacity>
            <TouchableOpacity onPress={handleSubmit}>
              <StyledButton>
                <StyledButtonText>{'Submit'}</StyledButtonText>
              </StyledButton>
            </TouchableOpacity>
          </ButtonContainer>
        </StyledForm>
      )}
    />
  );
};

export default RegisterFrom;
