export interface IRegisterSubmitValue {
  email: string;
  name: string;
  password: string;
}
