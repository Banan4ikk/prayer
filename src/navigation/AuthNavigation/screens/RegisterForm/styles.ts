import styled from 'styled-components/native';

export const StyledForm = styled.View`
  margin-top: 30%;
  display: flex;
  height: 400px;
  width: auto;
  justify-content: center;
  align-items: center;
`;
export const StyledButton = styled.View`
  background-color: #bfb393;
  height: 50px;
  width: 180px;
  border-radius: 30px;
  display: flex;
  justify-content: center;
  align-items: center;
`;
export const StyledButtonText = styled.Text`
  color: #fff;
  font-weight: bold;
  font-size: 18px;
`;
export const ButtonContainer = styled.View`
  margin-top: 15px;
  height: 100px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;
export const SignUpText = styled.Text`
  font-size: 16px;
  font-weight: bold;
  text-decoration: underline;
`;

export const StyledInput = styled.TextInput`
  border-radius: 30px;
  width: 300px;
  border: 2px solid #e5e5e5;
  padding: 15px;
  font-size: 18px;
  color: #c8c8c8;
  margin-top: 20px;
`;
