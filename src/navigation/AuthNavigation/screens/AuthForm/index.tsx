import React from 'react';
import {Field, Form} from 'react-final-form';
import {TouchableOpacity} from 'react-native';
import {
  ButtonContainer,
  SignUpText,
  StyledButton,
  StyledButtonText,
  StyledForm,
  StyledInput,
} from './styles';
import {useDispatch} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
import {REGISTER_ROUTE} from '../../routes';
import {IAuthSubmitValue} from './interfaces';
import {actions} from '../../../../redux/ducks';
import {NavigationAuthProps} from './types';

const AuthForm = () => {
  const navigation = useNavigation<NavigationAuthProps>();
  const dispatch = useDispatch();

  const onSubmit = (payload: IAuthSubmitValue) => {
    dispatch({type: actions.auth.signIn.type, payload});
  };

  const onClickText = () => {
    navigation.navigate(REGISTER_ROUTE);
  };

  return (
    <Form
      onSubmit={onSubmit}
      render={({handleSubmit}) => (
        <StyledForm>
          <Field name="email" type="email" placeholder="Email">
            {({input, placeholder}) => {
              return (
                <StyledInput
                  onChange={input.onChange}
                  placeholder={placeholder}
                  value={input.value}
                  onSubmitEditing={handleSubmit}
                />
              );
            }}
          </Field>
          <Field name="password" type="password" placeholder="Password">
            {({input, placeholder}) => {
              return (
                <StyledInput
                  onChange={input.onChange}
                  placeholder={placeholder}
                  value={input.value}
                  onSubmitEditing={handleSubmit}
                />
              );
            }}
          </Field>
          <ButtonContainer>
            <TouchableOpacity onPress={onClickText}>
              <SignUpText>{'Sign Up'}</SignUpText>
            </TouchableOpacity>
            <TouchableOpacity onPress={handleSubmit}>
              <StyledButton>
                <StyledButtonText>{'Submit'}</StyledButtonText>
              </StyledButton>
            </TouchableOpacity>
          </ButtonContainer>
        </StyledForm>
      )}
    />
  );
};

export default AuthForm;
