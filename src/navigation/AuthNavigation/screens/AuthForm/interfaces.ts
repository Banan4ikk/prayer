export interface IAuthSubmitValue {
  email: string;
  password: string;
}
