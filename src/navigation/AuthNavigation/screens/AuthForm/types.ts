import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import {RootStackParamList} from '../../../../types';

export type NavigationAuthProps = NativeStackNavigationProp<
  RootStackParamList,
  'register'
>;
