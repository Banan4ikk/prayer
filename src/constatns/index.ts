export const API_URL = 'https://prayer.herokuapp.com';
export const REGISTER_URL = '/auth/sign-up';
export const SIGN_IN_URL = '/auth/sign-in';
export const COLUMNS_URL = '/columns';
export const PRAYERS_URL = '/prayers';
export const COMMENTS_URL = '/comments';
