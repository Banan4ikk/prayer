import React from 'react';
import {Provider} from 'react-redux';
import {persistor, store} from './redux/store';
import {PersistGate} from 'redux-persist/integration/react';
import StackApp from './navigation/StackApp';

const App = () => {
  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <StackApp />
      </PersistGate>
    </Provider>
  );
};

export default App;
