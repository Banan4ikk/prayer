import {rootState} from '../store';
import {createSelector} from '@reduxjs/toolkit';

export const selectComments = (state: rootState) => state.comments;

export const getCommentsByPrayerId = (prayerId: number) =>
  createSelector(selectComments, state =>
    state.filter(item => item.prayerId === prayerId),
  );
