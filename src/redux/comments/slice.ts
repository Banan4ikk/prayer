import {IComment, IGetCommentResponse} from './types';
import {createSlice, PayloadAction} from '@reduxjs/toolkit';

const initialState: IComment[] = [] as IComment[];

const commentSlice = createSlice({
  name: 'comments',
  initialState,
  reducers: {
    getComments() {},
    deleteComment() {},
    changeComment() {},
    addComment() {},
    getCommentsSuccess(state, {payload}: PayloadAction<IComment[]>) {
      return payload;
    },
    deleteCommentSuccess(state, {payload}: PayloadAction<number>) {
      const index = state.findIndex(item => item.id === payload);

      if (index !== -1) {
        state.splice(index, 1);
      }
      return state;
    },
    changeCommentSuccess(state, {payload}: PayloadAction<IComment>) {
      if (payload) {
        const {id, body} = payload;
        const editComment = state.find(item => item.id === id);

        if (editComment) {
          editComment.body = body;
        }
        return state;
      }
    },
    addCommentSuccess(state, {payload}: PayloadAction<IGetCommentResponse>) {
      const {id, body, created, prayerId, userId} = payload;
      const newComment: IComment = {
        id,
        body,
        created,
        prayerId,
        userId,
      };
      if (payload) {
        state.push(newComment);
      }

      return state;
    },
  },
});

const actions = {...commentSlice.actions};
const reducer = commentSlice.reducer;

export {actions, reducer};
