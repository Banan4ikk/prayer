import {IPrayer} from '../prayers/types';
import {IAuthResponse} from '../auth/types';

export interface IComment {
  id: number;
  body: string;
  created: string;
  prayerId: number;
  userId: number;
}

export interface IGetCommentResponse extends IComment {
  card: IPrayer;
  user: IAuthResponse;
}

export interface IChangeComment {
  commentId: number;
  body: string;
}
