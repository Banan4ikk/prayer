import {call, put, takeEvery} from 'redux-saga/effects';
import {actions} from '../ducks';
import {AxiosResponse} from 'axios';
import {http} from '../../services/Http';
import {COMMENTS_URL} from '../../constatns';
import {PayloadAction} from '@reduxjs/toolkit';
import {IChangeComment, IComment} from './types';

export function* watchComments() {
  yield takeEvery(actions.comments.getComments.type, fetchComments);
}

export function* fetchComments() {
  try {
    const {data}: AxiosResponse = yield call(() => http.get(COMMENTS_URL));
    yield put(actions.comments.getCommentsSuccess(data));
  } catch (error) {
    console.log('error: ', error);
  }
}

export function* watchAddComments() {
  yield takeEvery(actions.comments.addComment.type, fetchAddComments);
}

export function* fetchAddComments({payload}: PayloadAction<IComment>) {
  try {
    const {data}: AxiosResponse = yield call(() =>
      http.post(COMMENTS_URL, payload),
    );
    yield put(actions.comments.addCommentSuccess(data));
  } catch (error) {
    console.log('error: ', error);
  }
}

export function* watchDeleteComment() {
  yield takeEvery(actions.comments.deleteComment.type, fetchDeleteComment);
}

export function* fetchDeleteComment({payload}: PayloadAction<number>) {
  try {
    const response: AxiosResponse = yield call(() =>
      http.delete(`${COMMENTS_URL}/${payload}`),
    );
    if (response.status === 200) {
      yield put(actions.comments.deleteCommentSuccess(payload));
    }
  } catch (error) {
    console.log('error', error);
  }
}

export function* watchChangeComment() {
  yield takeEvery(actions.comments.changeComment.type, fetchChangeComment);
}

export function* fetchChangeComment({payload}: PayloadAction<IChangeComment>) {
  console.log('payload', payload);
  const {commentId, body} = payload;
  try {
    const {data}: AxiosResponse = yield call(() =>
      http.put(`${COMMENTS_URL}/${commentId}`, {body}),
    );
    yield put(actions.comments.changeCommentSuccess(data));
  } catch (error) {
    console.log('error', error);
  }
}
