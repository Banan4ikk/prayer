import {call, put, takeEvery} from 'redux-saga/effects';
import {actions} from '../ducks';
import {AxiosResponse} from 'axios';
import {http} from '../../services/Http';
import {COLUMNS_URL} from '../../constatns';
import {IAddColumn} from './types';
import {PayloadAction} from '@reduxjs/toolkit';
import {IColumnChange} from '../../navigation/UserNavigation/screens/Desk/interfaces';

export function* watchColumns() {
  yield takeEvery(actions.columns.getColumnData, fetchColumns);
}

export function* fetchColumns() {
  try {
    const {data}: AxiosResponse = yield call(() => http.get(COLUMNS_URL));
    yield put(actions.columns.setColumnData(data));
  } catch (error) {
    console.log('error: ', error);
  }
}

export function* watchAddColumns() {
  yield takeEvery(actions.columns.addColumn.type, fetchAddColumns);
}

export function* fetchAddColumns({payload}: PayloadAction<IAddColumn>) {
  try {
    const {data}: AxiosResponse = yield call(() =>
      http.post(COLUMNS_URL, payload),
    );
    yield put(actions.columns.addColumnSuccess(data));
  } catch (error) {
    console.log('error: ', error);
  }
}

export function* watchDeleteColumns() {
  yield takeEvery(actions.columns.deleteColumn.type, fetchDeleteColumns);
}

export function* fetchDeleteColumns({payload}: PayloadAction<number>) {
  try {
    yield call(() => http.delete(`${COLUMNS_URL}/${payload}`));
    yield put(actions.columns.deleteColumnSuccess(payload));
  } catch (error) {
    console.log('error: ', error);
  }
}

export function* watchChangeColumn() {
  yield takeEvery(actions.columns.changeTitle.type, fetchChangeColumns);
}

export function* fetchChangeColumns({payload}: PayloadAction<IColumnChange>) {
  const {columnId, requestData} = payload;
  try {
    const {data}: AxiosResponse = yield call(() =>
      http.put(`${COLUMNS_URL}/${columnId}`, requestData),
    );
    yield put(actions.columns.changeTitleSuccess(data));
  } catch (error) {
    console.log('error: ', error);
  }
}
