import {rootState} from '../store';
import {createSelector} from '@reduxjs/toolkit';

export const getColumns = (state: rootState) => state.columns;

export const getColumnDescById = (columnId: number) =>
  createSelector(getColumns, state => {
    return state.find(item => item.id === columnId)?.description;
  });
