import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {IChangeColumnResponse, IColumnData} from './types';

const initialState: IColumnData[] = [] as IColumnData[];

const columnSlice = createSlice({
  name: 'columns',
  initialState,
  reducers: {
    getColumnData() {},
    addColumn() {},
    deleteColumn() {},
    changeTitle() {},
    addColumnSuccess(state, {payload}: PayloadAction<IColumnData>) {
      if (payload) {
        state.push(payload);
      }
      return state;
    },
    setColumnData(state, {payload}: PayloadAction<IColumnData[]>) {
      return payload;
    },
    deleteColumnSuccess(state, {payload}: PayloadAction<number>) {
      const index = state.findIndex(item => item.id === payload);

      if (index !== -1) {
        state.splice(index, 1);
      }
      return state;
    },
    changeTitleSuccess(state, {payload}: PayloadAction<IChangeColumnResponse>) {
      const {id, title} = payload;
      const editCard = state.find(item => item.id === id);

      if (editCard) {
        editCard.title = title;
      }

      return state;
    },
  },
});

const actions = {...columnSlice.actions};
const reducer = columnSlice.reducer;

export {actions, reducer};
