export type IColumnData = {
  id: number;
  title: string;
  description: string;
  userId: number;
};

export type IAddColumn = {
  title: string;
  description: string | null;
  userId: number;
};

export type IChangeColumnResponse = {
  id: number;
  title: string;
  description: string;
  userId: number;
};
