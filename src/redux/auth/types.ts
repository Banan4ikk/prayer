export type IAuthResponse = {
  id: number;
  email: string;
  name: string;
  token: string;
};

export type AuthData = {
  email: string;
  password: string;
};

export type RegisterData = {
  email: string;
  name: string;
  password: string;
};
