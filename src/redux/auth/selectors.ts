import {rootState} from '../store';

export const getToken = (state: rootState) => state.auth.token;
export const getUserId = (state: rootState) => state.auth.id;
export const getUserName = (state: rootState) => state.auth.name;
