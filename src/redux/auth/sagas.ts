import {http} from '../../services/Http';
import {call, put, takeEvery} from 'redux-saga/effects';
import {actions} from '../ducks';
import {REGISTER_URL, SIGN_IN_URL} from '../../constatns';
import {PayloadAction} from '@reduxjs/toolkit';
import {AxiosResponse} from 'axios';
import {AuthData, RegisterData} from './types';

export function* watchSignIn() {
  yield takeEvery(actions.auth.signIn.type, fetchSignIn);
}

export function* fetchSignIn(action: PayloadAction<AuthData>) {
  try {
    const {payload} = action;
    const {data}: AxiosResponse = yield call(() =>
      http.post(SIGN_IN_URL, payload),
    );
    yield put(actions.auth.signInSuccess(data));
  } catch (error) {
    yield put(actions.auth.signInReject());
  }
}

export function* watchSignUp() {
  yield takeEvery(actions.auth.signUp.type, fetchSignUp);
}

export function* fetchSignUp(action: PayloadAction<RegisterData>) {
  try {
    const {payload} = action;
    const {data}: AxiosResponse = yield call(() =>
      http.post(REGISTER_URL, payload),
    );
    yield put(actions.auth.signUpSuccess(data));
    yield put(actions.columns.setColumnData(data.columns));
  } catch (error) {
    yield put(actions.auth.signUpReject());
  }
}
