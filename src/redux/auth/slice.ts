import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {IAuthResponse} from './types';

const initialState: IAuthResponse = {} as IAuthResponse;

const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    signIn() {},
    signUp() {},
    signInSuccess(state, {payload}: PayloadAction<IAuthResponse>) {
      return payload;
    },
    signUpSuccess(state, {payload}: PayloadAction<IAuthResponse>) {
      return payload;
    },
    signInReject(state) {
      state.token = '';

      return state;
    },
    signUpReject(state) {
      state.token = '';

      return state;
    },
    signOut() {
      return initialState;
    },
  },
});

const actions = {...authSlice.actions};
const reducer = authSlice.reducer;

export {actions, reducer};
