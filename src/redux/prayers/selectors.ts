import {rootState} from '../store';
import {createSelector} from '@reduxjs/toolkit';

export const selectPrayers = (state: rootState) => state.prayers;

export const getPrayersByColumnId = (columnId: number) =>
  createSelector(selectPrayers, state =>
    state.filter(item => item.columnId === columnId),
  );

export const getPrayerDataById = (prayerId: number) =>
  createSelector(selectPrayers, state => {
    const prayer = state.find(item => item.id === prayerId);
    return {
      title: prayer?.title,
      description: prayer?.description,
      checked: prayer?.checked,
      commentsIds: prayer?.commentsIds,
    };
  });
