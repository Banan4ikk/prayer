import {IColumnData} from '../column/types';

export type IPrayer = {
  id: number;
  title: string;
  description: string;
  checked: boolean;
  columnId: number;
  commentsIds: [string];
};

export type IPrayerResponse = {
  id: number;
  title: string;
  description: string;
  checked: boolean;
  column: IColumnData;
  commentsIds: [string];
};

export type IChangePrayerResponse = {
  id: number;
  title: string;
  description: string;
  userId: number;
};
