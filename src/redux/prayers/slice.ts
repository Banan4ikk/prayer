import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {IChangePrayerResponse, IPrayer, IPrayerResponse} from './types';

const initialState: IPrayer[] = [] as IPrayer[];

const prayersSlice = createSlice({
  name: 'prayers',
  initialState,
  reducers: {
    getPrayers() {},
    addPrayer() {},
    deletePrayer() {},
    changePrayer() {},
    setPrayers(state, {payload}: PayloadAction<IPrayer[]>) {
      return payload;
    },
    addPrayerSuccess(state, {payload}: PayloadAction<IPrayerResponse>) {
      const {id, title, description, checked, column, commentsIds} = payload;
      const columnId = column.id;
      const newPrayerObj: IPrayer = {
        id,
        title,
        description,
        checked,
        columnId,
        commentsIds,
      };
      if (payload) {
        state.push(newPrayerObj);
      }
      return state;
    },
    deletePrayerSuccess(state, {payload}: PayloadAction<number>) {
      const index = state.findIndex(item => item.id === payload);

      if (index !== -1) {
        state.splice(index, 1);
      }
      return state;
    },
    changePrayerSuccess(
      state,
      {payload}: PayloadAction<IChangePrayerResponse>,
    ) {
      const {id, title, description} = payload;
      const editPrayer = state.find(item => item.id === id);

      if (editPrayer) {
        editPrayer.title = title;
        editPrayer.description = description;
      }

      return state;
    },
  },
});

const actions = {...prayersSlice.actions};
const reducer = prayersSlice.reducer;

export {actions, reducer};
