import {call, put, takeEvery} from 'redux-saga/effects';
import {actions} from '../ducks';
import {AxiosResponse} from 'axios';
import {http} from '../../services/Http';
import {PRAYERS_URL} from '../../constatns';
import {IPrayer} from './types';
import {PayloadAction} from '@reduxjs/toolkit';
import {IChangePrayer} from '../../navigation/UserNavigation/screens/PrayerDesk/interfaces';

export function* watchPrayers() {
  yield takeEvery(actions.prayers.getPrayers.type, fetchPrayers);
}

export function* fetchPrayers() {
  try {
    const {data}: AxiosResponse = yield call(() => http.get(PRAYERS_URL));
    yield put(actions.prayers.setPrayers(data));
  } catch (error) {
    console.log('error: ', error);
  }
}

export function* watchAddPrayers() {
  yield takeEvery(actions.prayers.addPrayer.type, fetchAddPrayers);
}

export function* fetchAddPrayers({payload}: PayloadAction<IPrayer>) {
  try {
    const {data}: AxiosResponse = yield call(() =>
      http.post(PRAYERS_URL, payload),
    );
    yield put(actions.prayers.addPrayerSuccess(data));
  } catch (error) {
    console.log('error: ', error);
  }
}

export function* watchDeletePrayers() {
  yield takeEvery(actions.prayers.deletePrayer.type, fetchDeletePrayers);
}

export function* fetchDeletePrayers({payload}: PayloadAction<number>) {
  try {
    const response: AxiosResponse = yield call(() =>
      http.delete(`${PRAYERS_URL}/${payload}`),
    );
    if (response.status === 200) {
      yield put(actions.prayers.deletePrayerSuccess(payload));
    }
  } catch (error) {
    console.log('error', error);
  }
}

export function* watchChangePrayers() {
  yield takeEvery(actions.prayers.changePrayer.type, fetchChangePrayer);
}

export function* fetchChangePrayer({payload}: PayloadAction<IChangePrayer>) {
  const {prayerId, requestData} = payload;
  try {
    const {data}: AxiosResponse = yield call(() =>
      http.put(`${PRAYERS_URL}/${prayerId}`, requestData),
    );
    yield put(actions.prayers.changePrayerSuccess(data));
  } catch (error) {
    console.log('error', error);
  }
}
