import createSagaMiddleware from 'redux-saga';
import {rootReducer} from './ducks';
import rootSaga from './sagas';
import {configureStore} from '@reduxjs/toolkit';
import {authMiddleware} from './middwares';

import persistReducer from 'redux-persist/es/persistReducer';
import persistStore from 'redux-persist/es/persistStore';
import {AsyncStorage} from 'react-native';

const sagaMiddleware = createSagaMiddleware();

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
};
const persistedReducer = persistReducer(persistConfig, rootReducer);

export const store = configureStore({
  reducer: persistedReducer,
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware({
      serializableCheck: false,
    }).concat(authMiddleware, sagaMiddleware),
});

sagaMiddleware.run(rootSaga);

export const persistor = persistStore(store);
export type rootState = ReturnType<typeof store.getState>;
