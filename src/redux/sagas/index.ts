import {all, fork} from 'redux-saga/effects';
import {watchSignIn, watchSignUp} from '../auth/sagas';
import {
  watchAddColumns,
  watchChangeColumn,
  watchColumns,
  watchDeleteColumns,
} from '../column/sagas';
import {
  watchAddPrayers,
  watchChangePrayers,
  watchDeletePrayers,
  watchPrayers,
} from '../prayers/sagas';
import {
  watchAddComments,
  watchChangeComment,
  watchComments,
  watchDeleteComment,
} from '../comments/sagas';

export default function* rootSaga() {
  yield all([
    fork(watchSignIn),
    fork(watchSignUp),
    fork(watchColumns),
    fork(watchPrayers),
    fork(watchAddPrayers),
    fork(watchDeletePrayers),
    fork(watchAddColumns),
    fork(watchDeleteColumns),
    fork(watchChangeColumn),
    fork(watchChangePrayers),
    fork(watchComments),
    fork(watchAddComments),
    fork(watchDeleteComment),
    fork(watchChangeComment),
  ]);
}
