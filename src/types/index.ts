export type RootStackParamList = {
  PrayersScreen: {columnId: number};
  HomeScreen: undefined;
  SubscribeScreen: undefined;
  PrayerDetails: {prayerId: number};
  auth: undefined;
  register: undefined;
};
